import { counterActions } from '../store/index';
import { useSelector, useDispatch } from 'react-redux';

import classes from './Counter.module.css';

const Counter = () => {
  // const [counter, setCounter] = useState(0)
  const dispatch = useDispatch();
  const counter = useSelector(state => state.counter);
  const show = useSelector(state => state.showCounter);

  const incrementNumber = () => {
    dispatch(counterActions.increment());
  }

  const increaseByUserInput = () => {
    dispatch(counterActions.increase(10));
  }

  const decrementNumber = () => {
    dispatch(counterActions.decrement());
  }

  const toggleCounterHandler = () => {
    dispatch(counterActions.toogleCounter());
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {show && <div className={classes.value}>{counter}</div>}
      <div>
        <button onClick={incrementNumber}>increment</button>
        <button onClick={increaseByUserInput}>increase by 7</button>
        <button onClick={decrementNumber}>decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
